import Vue from 'vue'
import Router from 'vue-router'
import vbclass from 'vue-body-class'

import Home from '../pages/Home'
import PageAllRecipes from '../pages/PageAllRecipes'
import PageCalendar from '../pages/PageCalendar'
import PageAddRecipe from '../pages/PageAddRecipe'
import PageLogin from '../pages/PageLogin'
import PageInscription from '../pages/PageInscription'
import PageProfile from '../pages/PageProfile'
import PageProfileSettings from '../pages/PageProfileSettings'
import PageDetailsRecipe from '../pages/PageDetailsRecipe'
import PageAllFarmers from '../pages/PageAllFarmers'

const VueRouter = new Router({
  routes: [
    {
      path: '*',
      name: 'Home',
      component: Home,
      meta: { bodyClass: 'home' }
    },
    {
      path: '/page-all-recipes',
      name: 'PageAllRecipes',
      component: PageAllRecipes,
      meta: { bodyClass: 'page-all-recipes' }
    },
    {
      path: '/page-all-farmers',
      name: 'PageAllFarmers',
      component: PageAllFarmers,
      meta: { bodyClass: 'page-all-farmers' }
    },
    {
      path: '/page-calendar',
      name: 'PageCalendar',
      component: PageCalendar,
      meta: { bodyClass: 'p-calendar' }
    },
    {
      path: '/recipes/new',
      name: 'PageAddRecipe',
      component: PageAddRecipe,
      meta: { bodyClass: 'p-add-recipe' }
    },
    {
      path: '/login',
      name: 'PageLogin',
      component: PageLogin,
      meta: { bodyClass: 'p-login' }
    },
    {
      path: '/login/new',
      name: 'PageInscription',
      component: PageInscription,
      meta: { bodyClass: 'p-inscription' }
    },
    {
      path: '/profile',
      name: 'PageProfile',
      component: PageProfile,
      meta: { bodyClass: 'p-profile' }
    },
    {
      path: '/settings',
      name: 'PageProfileSettings',
      component: PageProfileSettings,
      meta: { bodyClass: 'p-profile-settings' }
    },
    {
      path: '/recipe/:id',
      name: 'PageDetailsRecipe',
      component: PageDetailsRecipe,
      props: true,
      meta: { bodyClass: 'p-details-recipe' }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default VueRouter

Vue.use(Router)
Vue.use(vbclass, VueRouter)
