import Vue from 'vue'
import Meta from 'vue-meta'
import VueFullPage from 'vue-fullpage.js'
import * as VueGoogleMaps from 'vue2-google-maps'
import router from './router'
import store from './store'
import App from './App'
import GmapCluster from 'vue2-google-maps/dist/components/cluster'
import Paginate from 'vuejs-paginate'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

import VModal from 'vue-js-modal'
Vue.use(VModal)
Vue.component('v-select', vSelect)

Vue.component('paginate', Paginate)

window.$ = require('jquery')
window.JQuery = require('jquery')

Vue.use(Meta)
Vue.use(VueFullPage)
Vue.component('GmapCluster', GmapCluster)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBVTQyyEoulOCyR-R3AMCzkG8DlJwe6Eso',
    libraries: 'places',
    region: 'VI',
  },
})

window.onload = () => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
