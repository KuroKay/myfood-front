import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    config: null,
    locale: 'en', // Default is english
    i18n: null
  },
  mutations: {
    storeConfig: function (state, config) {
      state.config = config
    },
    storeLocale: function (state, lang) {
      state.locale = lang
    },
    storeI18n: function (state, i18n) {
      state.i18n = i18n
    }
  }
})
